fn binary_search(vec: &Vec<i32>, n: i32) -> i32 {
    let (mut l, mut r) = (0, vec.len());
    while l <= r {
        let m: usize = (l + r) / 2;
        if vec[m] == n {
            return m as i32;
        } else if vec[m] > n {
            r = m - 1;
        } else {
            l = m + 1;
        }
    }
    return -1;
}

fn binary_search_recursion(vec: &Vec<i32>, l: usize, r: usize, n: i32) -> i32 {
    if l > r {
        return -1;
    }
    let m = (l + r) / 2;
    if vec[m] > n {
        return binary_search_recursion(vec, l, m - 1, n);
    } else if vec[m] < n {
        return binary_search_recursion(vec, m + 1, r, n);
    }
    return m as i32;
}

fn main() {
    let vec = [1, 2, 3, 5, 6].to_vec();
    let n = 3;
    let ret = binary_search(&vec, n);
    let rret = binary_search_recursion(&vec, 0, vec.len(), n);
    println!("{:?}\n{:?}", ret, rret);
}


