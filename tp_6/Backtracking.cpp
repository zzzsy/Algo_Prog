#include <iostream>
#include <stdlib.h>
using namespace std;

// question 1
void updateState(int *s[], int pi, int pj, int n) {
  int i = pi;
  while (i < n) {
    s[i][pj] = 0;
    if (pj + pi - i >= 0)
      s[i][pj + pi - i] = 0;
    if (pj - pi + i < n)
      s[i][pj - pi + i] = 0;
    i++;
  }
}

// question 2
int empty(int s[], int n) {
  for (int i = 0; i < n; i++)
    if (s[i])
      return s[i];
  return 0;
}
// question 3
void solveNqueen(int n) {

  int S[n][n];     // state array
  int solution[n]; // solution vector
  int counter = 0; // the solution number counter
  int i, j;
  int k = 0;

  // initial state array

  for (i = 0; i < n; i++) {
    solution[i] = 0;
    for (j = 0; j < n; j++) {
      S[i][j] = j + 1;
    }
  }
  int *(s[n]);
  for (int i = 0; i < n; i++)
    s[i] = S[i];

  // start the search

  do {

    // recover the k line' state when we backtrack to k-1

    for (i = k + 1; i < n; i++) {
      for (j = 0; j < n; j++) {
        S[i][j] = j + 1;
      }
    }
    for (i = 0; i < k; i++) {
      updateState(s, i, solution[i] - 1, n);
    }

    // the main body of searching

    while (empty(S[k], n) != 0) {
      solution[k] = empty(S[k], n);
      S[k][solution[k] - 1] = 0;
      updateState(s, k, solution[k] - 1, n);
      // update the state according to the current selection
      if (k < n - 1)
        k += 1; // to the next level
      else {
        cout << "Find a solution!" << endl;
        for(auto n:solution){
          cout<<n<<" ";
        }
        cout<<endl;
        counter++;
        break;
      }
    }
    k -= 1; // backtracking
  } while (k >= 0);

  cout << "Solution is: " << counter << endl;
}

int main(int argc, char **argv) {
  // read parameters from command line.
  // If the user don't give the parameter n, then print the error message and
  // return.
  if (argc != 2) {
    cout << "please input n!" << endl;
    return 1;
  }

  // convert the input parameters which are string to integer
  int n = atoi(*(++argv));
  cout << "your input n is: " << n << endl;
  solveNqueen(n);
}

// The solution of N queens problem
// n       solution(n)
//  1       1
//  2       0
//  3       0
//  4       2
//  5       10
//  6       4
//  7       40
//  8       92
//  9       352
//  10      724
