#include <iostream>
#include <time.h>
#include<vector>
using namespace std;

// question 1

int uniquePaths(int m, int n) {
  vector<vector<int>> vec(m, vector<int>(n, 1));
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      if (i > 0 && j > 0) {
        vec[i][j] = vec[i][j - 1] + vec[i - 1][j];
      } else if (i > 0) {
        vec[i][j] = vec[i - 1][j];
      } else if (j > 0) {
        vec[i][j] = vec[i][j - 1];
      }
    }
  }
  return vec[m - 1][n - 1];
}

// question 2
static int a[100][100] = {0};
int uniquePathsRe(int m, int n) {
  if (m <= 0 || n <= 0)
    return 0;
  else if (m == 1 || n == 1)
    return 1;
  else if (m == 2 && n == 2)
    return 2;
  if (a[m][n] > 0)
    return a[m][n];
  a[m - 1][n] = uniquePathsRe(m - 1, n);
  a[m][n - 1] = uniquePathsRe(m, n - 1);
  a[m][n] = a[m - 1][n] + a[m][n - 1];
  return a[m][n];
}
// question 3
int uniquePathsSimple(int m, int n) {
  vector<int> dp(n, 1);
  for (int i = 1; i < m; i++) {
    for (int j = 1; j < n; j++) {
      dp[j] = dp[j] + dp[j - 1];
    }
  }
  return dp[n - 1];
}

// test code
int main(int argc, char **argv) {
  // read parameters from command line.
  // If the number of input parameters isn't two, then print the error message
  // and return.
  if (argc != 3) {
    cout << "please input m and n!" << endl;
    return 1;
  }

  // convert the input parameters which are string to integer
  int m = atoi(*(++argv));
  int n = atoi(*(++argv));
  cout << m << "," << n << endl;

  // save the start time and end time for the comparison
  clock_t start, end;
  long r;
  start = clock();
  // r = uniquePaths(m, n); // question 1 test
//   r = uniquePathsRe(m,n);//question 2 test
  r = uniquePathsSimple(m,n);//question 3 test
  end = clock();
  cout << r << endl;
  cout << "time used: " << (double)(end - start) / CLOCKS_PER_SEC * 1000 << "ms"
       << endl;
}

// test case used:
// m, n 	result
// 7,3		28
// 20,10		6906900
// 30,10		163011640
// 1,1 		1
