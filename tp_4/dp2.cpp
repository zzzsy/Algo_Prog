#include <cstdio>
#include <iostream>
#include <vector>
using namespace std;

// question 4 and 5
int uniquePathsWithObstacles(int **obstacleGrid, int m, int n) {
  vector<int> dp(n, 0);
  dp[0] = 1;
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      if (obstacleGrid[i][j] == 1)
        dp[j] = 0;
      else if (j > 0)
        dp[j] += dp[j - 1];
    }
  }
  return dp[n - 1];
}

int main() {
  int m = 1;
  int n = 1;
  int obs[1][1] = {{1}};
  // convert obstacles array to int **
  int *p[m];
  for (int i = 0; i < m; i++)
    p[i] = obs[i];

  int r = uniquePathsWithObstacles(p, m, n);
  cout << r << endl;
}

// test case used:
// m, n      obs                              result
// 3,3       [[0,0,0],[0,1,0],[0,0,0]]           2
// 1,2       [[0,1]]                             0
// 3,2       [[0,0],[1,1],[0,0]]                 0
// 1,1       [[1]]                               0
