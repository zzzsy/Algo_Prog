#include <cstdio>
#include <iostream>
#include <vector>
using namespace std;

int maxCoins(int *nums, int numsSize) {
  vector<int> p(numsSize + 2, 1); // 1  3 1 5 8  1
  for (int i = 1; i <= numsSize; i++)
    p[i] = nums[i - 1];

  vector<vector<int>> f(numsSize + 2, vector<int>(numsSize + 2, 0));
  for (int len = 3; len <= numsSize + 2; len++) {
    for (int i = 0; i + len - 1 <= numsSize + 1; i++) {
      int j = i + len - 1;
      int tmp = p[i] * p[j];
      for (int k = i + 1; k <= j - 1; k++) {
        f[i][j] = max(f[i][j], f[i][k] + f[k][j] + p[k] * tmp);
      }
    }
  }

  return f[0][numsSize + 1];
}

int main() {
  // example 1
  int numsSize = 4;
  int nums[4] = {3, 1, 5, 8};

  // example 2
  //   int numsSize = 2;
  //   int nums[2] = {1, 5};

  int coinNumber = 0;

  coinNumber = maxCoins(nums, numsSize);
  cout << "the max coin number is: " << coinNumber << endl;
  return 0;
}

// for example 1, the result is 167
// for example 2, the result is 10
