#include <iostream>
#include <math.h>
using namespace std;

int main() {
  int n;
  double uprec, ucour, usuiv;
  cout << "Veuillez entrer le numéro N" << endl;
  cin >> n;

  uprec = usuiv = 1;
  double r = (1. - sqrt(5)) / 2.;
  ucour = r;

  for (int i = 2; i <= n; i = i + 1) {
    usuiv = ucour + uprec;
    uprec = ucour;
    ucour = usuiv;

    double ui_par_r = pow(r, i);

    cout << "par récu, u" << i << "=" << ucour << "\t";
    cout << "par r, u" << i << "=" << ui_par_r << endl;
  }
  return 0;
}
