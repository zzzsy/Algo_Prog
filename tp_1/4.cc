#include <iostream>
using namespace std;

int main() {
  // variables locales
  int valeur;
  int nbs[6] = {0};
  int nbLancers = 0;
  float fs[6] = {0};

  cout << "Calcul des frequences d'obtention d'un jeu de des" << endl;
  cout << "Entrer une valeur :";
  cin >> valeur;

  while ((valeur > 0) && (valeur < 7)) {
    nbs[valeur - 1] += 1;
    nbLancers = nbLancers + 1;
    cout << "Entrer une valeur :";
    cin >> valeur;
  }
  cout << "Frequences :" << endl;
  if (nbLancers > 0) {
    for (int i = 0; i < 6; i++) {
      fs[i] = (float)nbs[i] / nbLancers;
      cout << "f" << i + 1 << "=" << fs[i] << endl;
    }
  } else {
    cout << "pas de valeurs entre 1 et 6" << endl;
  }
  return 0;
}
