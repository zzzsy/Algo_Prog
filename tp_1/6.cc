#include <cstdlib>
#include <ctime>
#include <iostream>
#include <stdlib.h>
using namespace std;

int main() {
  int n;
  int position = 0;
  srand((unsigned)time(NULL));
  while (position != 63 && position != 52) {
    n = rand() % 6 + 1;
    position += n;
    if (position <= 63) {
      cout << "+" << n;
      switch (position) {
      case 9:
      case 18:
      case 27:
      case 36:
      case 45:
      case 54: {
        position += n;
        cout << "+" << n;
        break;
      }
      }
      cout << "\t" << position << endl;
    }
    if (position > 63) {
      cout << "+" << 63 + n - position;
      position = 63 * 2 - position;
      cout << "-" << 63 - position << "\t" << position << endl;
    }
  }
  if (position == 63) {
    cout << "arrivée" << endl;
  }
  if (position == 52) {
    cout << "prison" << endl;
  }
}
