#ifndef __OPERATIONS_H__
#define __OPERATIONS_H__

#include "utilitaires.hpp"

t_EntierLong add_t(t_EntierLong n1, t_EntierLong n2);
t_EntierLong sous_t(t_EntierLong n1, t_EntierLong n2);
t_EntierLong add(t_EntierLong,t_EntierLong);
t_EntierLong sous(t_EntierLong,t_EntierLong);
t_EntierLong fois_t(t_EntierLong n1, t_EntierLong n2,int nn1,int nn2);
t_EntierLong fois(t_EntierLong,t_EntierLong);

#endif
