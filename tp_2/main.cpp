#include "operations.hpp"
#include "utilitaires.hpp"

void q1(int a, int b) {
  t_EntierLong n1 = conversion(a);
  t_EntierLong n2 = conversion(b);
  t_EntierLong ret = fois(n1, n2);
  AfficheEntierLong(ret);
}

void q2(int n) {
  t_EntierLong n1 = conversion(0);
  t_EntierLong n2 = conversion(1);
  t_EntierLong ret = conversion(0);
  printf("u0\t");
  AfficheEntierLong(n1);
  printf("u1\t");
  AfficheEntierLong(n2);
  for (int i = 0; i < n - 1; i++) {
    ret = add(n1, n2);
    printf("u%d\t", i + 2);
    AfficheEntierLong(ret);
    n1 = n2;
    n2 = ret;
  }
  printf("u60-u59=u58\n");
}

void q4() {
  t_EntierLong n1 = conversion(83928);
  t_EntierLong n2 = conversion(2541);
  t_EntierLong n3 = conversion(98712);
  t_EntierLong ret = fois(n1, n2);
  ret = fois(ret, n3);
  printf("83928*2541*98712=");
  AfficheEntierLong(ret);
}

void q5(){
  int n=521;
  t_EntierLong ret=conversion(2);
  for(int i=0;i<n-1;i++){
    ret=conversion(ret.Chiffres[0]*2);
  }
  ret=sous(ret,conversion(1));
  printf("le dernier chiffre de (2^521-1)=%d\n",ret.Chiffres[0]);
}

int main() {
  q1(23, -101);
  q2(60);
  q4();
  q5();
}
