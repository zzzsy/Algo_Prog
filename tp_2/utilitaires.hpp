#ifndef __UTILITAIRES_H__
#define __UTILITAIRES_H__

#include<stdio.h>
#include<iostream>
#include<math.h>
#include"entierlong.h"
#include"lit_ecrit.h"
using namespace std; 

t_EntierLong conversion(int);
bool egalEntierLong(t_EntierLong, t_EntierLong);
bool inferieurEgalPos(t_EntierLong, t_EntierLong);
int getCh(t_EntierLong n);
t_EntierLong newEn();

#endif
