#include "utilitaires.hpp"
#include "lit_ecrit.h"

t_EntierLong newEn(){
  t_EntierLong ret;
  for(int i=0;i<MAXCHIFFRES;i++){
    ret.Chiffres[i]=0;
  }
  ret.Negatif=false;
  return ret;
}

t_EntierLong conversion(int n) {
  t_EntierLong ret=newEn();
  if (n < 0) {
    ret.Negatif = true;
    n = -n;
  } else {
    ret.Negatif = false;
  }
  int tmp = 0;
  while (n != 0 && tmp < MAXCHIFFRES) {
    ret.Chiffres[tmp++] = n % 10;
    n /= 10;
  }
  return ret;
};

bool egalEntierLong(t_EntierLong n1, t_EntierLong n2) {
  for (int i = 0; i < MAXCHIFFRES; i++) {
    if (n1.Chiffres[i] != n2.Chiffres[i]) {
      return false;
    }
  }
  return true;
};
bool inferieurEgalPos(t_EntierLong n1, t_EntierLong n2) {

  for (int i = MAXCHIFFRES - 1; i >= 0; i--) {
    // printf("n1=%d\tn2=%d\n",n1.Chiffres[i],n2.Chiffres[i]);
    if (n1.Chiffres[i] < n2.Chiffres[i]) {
      return true;
    } else if (n1.Chiffres[i] > n2.Chiffres[i]) {
      return false;
    }
  }
  return false;
};

int getCh(t_EntierLong n) {
  for (int i = MAXCHIFFRES - 1; i >= 0; i--) {
    if (n.Chiffres[i] != 0) {
      return i + 1;
    }
  }
  return 0;
}
