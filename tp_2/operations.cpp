#include "operations.hpp"
#include "utilitaires.hpp"
#include <iostream>
#include <system_error>

t_EntierLong add_t(t_EntierLong n1, t_EntierLong n2) {
  t_EntierLong ret = newEn();
  int tmp1 = 0, tmp2 = 0;
  for (int i = 0; i < MAXCHIFFRES; i++) {
    tmp1 = n1.Chiffres[i] + n2.Chiffres[i] + tmp2;
    ret.Chiffres[i] = tmp1 % 10;
    tmp2 = tmp1 / 10;
  }
  if (tmp2 != 0) {
    cerr << "Calcul des résultats au-delà de 20 chiffres" << endl;
    exit(1);
  }
  return ret;
}

t_EntierLong sous_t(t_EntierLong n1, t_EntierLong n2) {
  if (inferieurEgalPos(n1, n2)) {
    exit(1);
  }
  t_EntierLong ret = newEn();
  int tmp1 = 0, tmp2 = 0;
  int ttmp = 0;
  for (int i = 0; i < MAXCHIFFRES; i++) {
    int tmp = n1.Chiffres[i] - n2.Chiffres[i] + ttmp;
    if (tmp >= 0) {
      ttmp = 0;
      ret.Chiffres[i] = tmp;
    } else {
      ret.Chiffres[i] = 10 + tmp;
      ttmp = -1;
    }
  }
  return ret;
}

t_EntierLong add(t_EntierLong n1, t_EntierLong n2) {
  t_EntierLong ret = newEn();
  if (n1.Negatif == n2.Negatif) {
    ret = add_t(n1, n2);
    ret.Negatif = n1.Negatif;
  } else {
    if (inferieurEgalPos(n1, n2)) {
      ret = sous_t(n2, n1);
      ret.Negatif = n2.Negatif;
    } else {
      ret = sous_t(n1, n2);
      ret.Negatif = n1.Negatif;
    }
  }
  return ret;
}

t_EntierLong sous(t_EntierLong n1, t_EntierLong n2) {
  t_EntierLong ret = newEn();
  if (n1.Negatif != n2.Negatif) {
    ret = add_t(n1, n2);
    ret.Negatif = n1.Negatif;
  } else {
    if (inferieurEgalPos(n1, n2)) {
      ret = sous_t(n2, n1);
      ret.Negatif = !n1.Negatif;
    } else {
      ret = sous_t(n1, n2);
      ret.Negatif = n1.Negatif;
    }
  }
  return ret;
}
t_EntierLong fois(t_EntierLong n1, t_EntierLong n2) {
  t_EntierLong ret = newEn();
  int nn1 = getCh(n1);
  int nn2 = getCh(n2);
  if ((nn1 + nn2) > MAXCHIFFRES) {
    cerr << "Calcul des résultats au-delà de 20 chiffres" << endl;
    exit(1);
  }
  if (nn1 > nn2) {
    ret = fois_t(n1, n2, nn1, nn2);
  } else {
    ret = fois_t(n2, n1, nn2, nn1);
  }
  ret.Negatif = n1.Negatif ^ n2.Negatif;
  return ret;
}

t_EntierLong fois_t(t_EntierLong n1, t_EntierLong n2, int nn1, int nn2) {
  t_EntierLong ret = newEn();
  int tmp = 0;
  for (int i = 0; i < nn2; i++) {
    t_EntierLong rret = newEn();
    for (int j = 0; j < nn1; j++) {
      rret.Chiffres[i + j] = (n2.Chiffres[i] * n1.Chiffres[j] + tmp) % 10;
      tmp = (n2.Chiffres[i] * n1.Chiffres[j] + tmp) / 10;
    }
    if (tmp > 0) {
      rret.Chiffres[i + nn1] = tmp;
    }
    ret = add_t(ret, rret);
  }
  return ret;
}
