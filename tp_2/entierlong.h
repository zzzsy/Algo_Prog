/*
fichier entierlong.h
pour définir le type t_EntierLong
*/

const int MAXCHIFFRES = 200;

struct t_EntierLong
{
	bool Negatif;
	int Chiffres[MAXCHIFFRES];
};
