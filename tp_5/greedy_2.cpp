#include <iostream>
#include <math.h>
using namespace std;

int candy(int ratings[], int n) {
  int cnt = 0;
  int nums[n];
  nums[0] = 1;
  for (int i = 1; i < n; i++) {
    nums[i] = 1;
    if (ratings[i] > ratings[i - 1]) {
      nums[i] = nums[i - 1] + 1;
    }
  }
  for (int i = n - 1; i >= 0; i--) {
    if (ratings[i] > ratings[i + 1]) {
      nums[i] = max(nums[i], nums[i + 1] + 1);
    }
    cnt += nums[i];
  }
  return cnt;
}

int main(void) {
  int n;
  cout << "请输入小朋友的人数：" << endl;
  cin >> n;
  int ratings[n];
  int rate;
  int i;
  for (i = 0; i < n; i++) {

    cout << "请输入第" << i + 1 << "个小朋友的分数：" << endl;
    cin >> rate;
    ratings[i] = rate;
  }

  int num_candy;
  num_candy = candy(ratings, n);

  cout << "最少需要的糖果个数为:" << num_candy;

  return 0;
}
