#include <iostream>
#include <math.h>
#include <algorithm>

using namespace std;

int findContentChildren(int n, int ratings[], int num_candy, int candies[]) {
  sort(ratings, ratings + n);
  sort(candies, candies + num_candy);
  int count = 0;
  for (auto num = candies; num != candies + num_candy; num++) {
    if (count < n && *num >= ratings[count]) {
      count++;
    }
  }
  return count;
}

int main() {
  int n;
  cout << "请输入小朋友的人数：" << endl;
  cin >> n;
  int ratings[n];
  int rate;
  int i;
  for (i = 0; i < n; i++) {

    cout << "请输入第" << i + 1 << "个小朋友的分数：" << endl;
    cin >> rate;
    ratings[i] = rate;
  }

  int num_candy;
  cout << "请输入糖果一共有几包：" << endl;
  cin >> num_candy;
  int candies[num_candy];
  int candy;
  for (i = 0; i < num_candy; i++) {

    cout << "请输入第" << i + 1 << "包糖果里有几个糖果：" << endl;
    cin >> candy;
    candies[i] = candy;
  }
  int content_children;
  content_children = findContentChildren(n, ratings, num_candy, candies);

  cout << "最多有几个小朋友得到满足:" << content_children;

  return 0;
}
