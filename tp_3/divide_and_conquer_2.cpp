#include <iostream>
using namespace std;

void printbl(int *tbl, int n) {
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      printf("%d ", *(tbl + i * n + j));
    }
    printf("\n");
  }
}

void GameTable(int k) {
  int n = 1 << k;
  int tbl[n][n];
  tbl[0][0] = 1;
  tbl[0][1] = 2;
  tbl[1][0] = 2;
  tbl[1][1] = 1;
  int m = 2;
  while (m != n) {
    for (int i = 0; i < m; i++) {
      for (int j = m; j < 2 * m; j++) {
        tbl[i][j] = tbl[i][j - m] + m;
      }
    }

    for (int i = m; i < 2 * m; i++) {
      for (int j = 0; j < m; j++) {
        tbl[i][j] = tbl[i - m][j] + m;
      }
      for (int j = m; j < 2 * m; j++) {
        tbl[i][j] = tbl[i][j - m] - m;
      }
    }
    m = m * 2;
  }
  printbl((int *)tbl, n);
}

int main(void) {
  cout << "共有2^k个选手参加比赛，输入k（k>0）：" << endl;
  int k;
  do {
    cin >> k;
  } while (k < 0 || k > 31);
  GameTable(k);

  return 0;
}
