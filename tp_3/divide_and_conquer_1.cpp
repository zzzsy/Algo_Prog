#include <cstdio>
#include <iostream>
using namespace std;

void printbl(int *tbl, int n) {
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      printf("%d ", *(tbl+i*n+j));
    }
	printf("\n");
  }
}

void GameTable(void) {
  int k = 1;
  int n = 1 << k;
  int tbl[n][n];
  tbl[0][0] = 1;
  tbl[0][1] = 2;
  tbl[1][0] = 2;
  tbl[1][1] = 1;
  printbl((int *)tbl, n);
}

int main(void) {

  GameTable();

  return 0;
}
