#include "listeentier.hpp"

t_ListeEntiers::t_ListeEntiers(type_name v) {
  tete = new t_Node(v);
  size = 1;
}

void t_ListeEntiers::insereDebut(type_name v) {
  auto *s = new t_Node(v);
  s->svt = tete;
  tete = s;
  size++;
}

void t_ListeEntiers::insereFin(type_name v) {
  t_Node *p = tete;
  while (p && p->svt)
    p = p->svt;
  p ? p->svt = new t_Node(v) : tete = new t_Node(v);
  size++;
}

void t_ListeEntiers::affichageListe() {
  t_Node *p = tete;
  std::cout << "[";
  while (p) {
    std::cout << p->value << ", ";
    p = p->svt;
  }
  std::cout << "...]" << std::endl;
}

bool t_ListeEntiers::siExiste(type_name v) {
  t_Node *p = tete;
  while (p != nullptr) {
    if (p->value == v)
      return true;
    p = p->svt;
  }
  return false;
}

void t_ListeEntiers::supTet() {
  if (size) {
    t_Node *p = tete;
    tete = tete->svt;
    size--;
    delete p;
  }
}

void t_ListeEntiers::supQue() {
  if (size) {
    t_Node *p = tete;
    while (p->svt && p->svt->svt)
      p = p->svt;
    if (p->svt == nullptr)
      tete = nullptr;
    t_Node *q = p->svt;
    p->svt = nullptr;
    delete q;
    size--;
  }
}

void t_ListeEntiers::dest() {
  while (!empty()) {
    supQue();
  }
  delete tete;
  tete = nullptr;
}
