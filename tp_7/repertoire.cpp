#include "repertoire.hpp"

void repertoire::inserePer(_strcref prenom, _strcref nom, _strcref tel) {
  personne *p = new personne(prenom, nom, tel);
  rep.push_back(*p);
};

void repertoire::affichageRepertoire() {
  cout << "Repertoire" << endl;
  for (auto p : rep)
    cout << p.prenom << "\t" << p.nom << "\t" << p.tel << endl;
}

void repertoire::obtTel(_strcref prenom, _strcref nom) {
  for (auto p = rep.begin(); p != rep.end(); p++) {
    if (p->prenom == prenom && p->nom == nom) {
      cout << p->tel << endl;
      return;
    }
  }
  cout << prenom << "" << nom << "n'exite pas" << endl;
}

bool repertoire::supPer(_strcref prenom, _strcref nom) {
  for (auto p = rep.begin(); p != rep.end(); p++) {
    if (p->prenom == prenom && p->nom == nom) {
      rep.erase(p);
      return true;
    }
  }
  return false;
}

void repertoire::insertionPersonne(_strcref prenom, _strcref nom, _strcref tel) {
  personne *p = new personne(prenom, nom, tel);
  auto per = rep.begin();
  while (per != rep.end())
    if (per->prenom > p->prenom ||
        (per->prenom == p->prenom && per->nom > p->nom))
      break;
    else
      per++;
  rep.insert(per, *p);
}

void repertoire::saveDirectory(_strcref s) {
  ofstream frep(s);
  for (auto p = rep.begin(); p != rep.end(); p++) {
    frep << p->prenom << "\t" << p->nom << "\t" << p->tel << endl;
  }
  frep.close();
}

void repertoire::readDirectory(_strcref s) {
  ifstream frep(s);
  if (!frep.is_open()) {
    cerr << "Error opening file";
    exit(1);
  }
  while (!frep.eof()) {
    personne *p = new personne;
    getline(frep, p->prenom, '\t');
    getline(frep, p->nom, '\t');
    getline(frep, p->tel, '\n');
    rep.push_back(*p);
  }
  frep.close();
}


