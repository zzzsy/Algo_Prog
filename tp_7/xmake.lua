target("listeentier")
    set_kind("static")
    add_files("listeentier.cpp")

target("repertoire")
    set_kind("static")
    add_files("repertoire.cpp")

target("listeentier_test")
    set_kind("binary")
    add_files("listeentier_test.cpp")
    add_deps("listeentier")

target("repertoire_test")
    set_kind("binary")
    add_files("repertoire_test.cpp")
    add_deps("repertoire")

