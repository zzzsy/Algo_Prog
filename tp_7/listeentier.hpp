#ifndef LISTEENTIER_HPP_
#define LISTEENTIER_HPP_
#include <cstddef>
#include <iostream>

using type_name = int;

class t_ListeEntiers {
  struct t_Node {
    type_name value;
    t_Node *svt;
    t_Node(type_name v = 0, t_Node *svt = nullptr) : value(v), svt(svt) {}
  };

private:
  t_Node *tete;
  type_name size;

public:
  t_ListeEntiers(type_name v);
  using creeListe = t_ListeEntiers;
  void insereDebut(type_name);
  void insereFin(type_name);
  void affichageListe();
  bool siExiste(type_name);
  bool empty() { return size == 0; };
  void supTet();
  void supQue();
  void dest();
  ~t_ListeEntiers() { dest(); };
};

#endif
