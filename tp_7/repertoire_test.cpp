#include "repertoire.hpp"

const string path = "./rep.txt";

// int main() {
//   repertoire l;
//   l.insertionPersonne("Zhang", "Mathias", "+1 (321) 701-5602");
//   l.insertionPersonne("Marie", "Hugo", "+1 (833) 806-1038");
//   l.insertionPersonne("Marie", "Adora", "+1 (650) 514-6402");
//   l.supPer("Madin", "Plouto");
//   l.saveDirectory();
//   l.insertionPersonne("Alixe", "Maxwell", "+1 (414) 533-5566");
//   l.affichageRepertoire();
//   l.dest();
//   l.readDirectory();
//   l.affichageRepertoire();
// }

int main() {
  char cmd;
  string prenom, nom, tel;
  repertoire::personne *per;
  repertoire rep;

  while (true) {
    cout << "\n\033[4mA\033[0mjouter\tA\033[4mf\033[0mfichager\n";
    cout << "\033[4mR\033[0mechercher\tSu\033[4mp\033[0mprimer\n";
    cout << "\033[4mS\033[0mauvegarder\t\033[4mL\033[0mire\t\033[4mQ\033["
            "0muit\n";
    cin >> cmd;
    switch (tolower(cmd)) {
    case 'a':
      cin >> prenom >> nom >> tel;
      rep.insertionPersonne(prenom, nom, tel);
      break;
    case 'f':
      rep.affichageRepertoire();
      break;
    case 'r':
      cin >> prenom >> nom;
      rep.obtTel(prenom, nom);
      break;
    case 'p':
      cin >> prenom >> nom;
      rep.supPer(prenom, nom);
      break;
    case 's':
      rep.saveDirectory(path);
      break;
    case 'l':
      rep.readDirectory(path);
      break;
    case 'q':
      return 0;
    default:
      cout << "Instructions non valides" << endl;
    }
  }
}
