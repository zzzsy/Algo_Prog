#ifndef REPERTOIRE_HPP_
#define REPERTOIRE_HPP_
#include <fstream>
#include <iostream>
#include <list>

using namespace std;
using _strcref = const string &;

class repertoire {
public:
  struct personne {
    string prenom, nom, tel;
    personne(string prenom = "", string nom = "", string tel = "")
        : prenom(prenom), nom(nom), tel(tel){};
  };
  list<personne> rep;

public:
  repertoire(){};
  void inserePer(_strcref prenom, _strcref nom, _strcref tel);
  void affichageRepertoire();
  void obtTel(_strcref, _strcref);
  bool supPer(_strcref, _strcref);
  void insertionPersonne(_strcref prenom, _strcref nom, _strcref tel);
  void saveDirectory(_strcref path="./rep.txt");
  void readDirectory(_strcref path="./rep.txt");
  void dest() { rep.clear(); };
};

#endif
