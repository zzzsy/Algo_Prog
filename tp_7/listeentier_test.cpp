#include "listeentier.hpp"

int main() {
  auto l = t_ListeEntiers::creeListe(3);
  l.supTet();
  l.insereDebut(2);
  l.insereFin(6);
  l.affichageListe();
  auto t = l.siExiste(3);
  std::cout << (t ? "3 existe" : "3 n'existe pas") << std::endl;
  l.dest();
  l.insereDebut(10);
  l.affichageListe();
}
